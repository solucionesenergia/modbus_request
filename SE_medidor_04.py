import pymodbus
import serial
import time
import numpy as np
from pymodbus.pdu import ModbusRequest
from pymodbus.client.sync import ModbusSerialClient as ModbusClient #initialize a serial RTU client instance
from pymodbus.transaction import ModbusRtuFramer
from datetime import datetime

#import logging
#logging.basicConfig()
#log = logging.getLogger()
#log.setLevel(logging.DEBUG)

#count= the number of registers to read
#unit= the slave unit this request is targeting
#address= the starting address to read from
#Agumentos de la funcion client.read_holding_register( Starting address, num of reg to read, slave unit ID)


client= ModbusClient(method = "rtu", port="/dev/ttyUSB0",stopbits = 1, bytesize = 8, parity = 'E', baudrate= 38400)

#Connect to the serial modbus server
connection = client.connect()
print (connection)



#Funcion para convertir datos de modbus a float32
def list_to_int(l):
    return np.dot(l,np.exp2(np.arange(len(l))))

def conversion(dato):
    #Creamos el arreglo bs que me separa individualmente los elementos del string
    bs=np.array(list(dato))
    
    #el comando b.astype covierte los elemento de la lista a enteros
    bs=bs.astype(int)
    sb = bs[::-1]
    
    s=sb[-1]
    f=sb[:23]
    e=sb[23:31]
    
    exponent = list_to_int(e)
    mantissa = list_to_int(f)*np.exp2(-len(f))
    dato_flotante = ((-1)**s)*(1+mantissa)*np.exp2(exponent-127)
    #print 'Valor de variable  medida es', dato_flotante
    return dato_flotante



#Declaracion de los archivos de escritura de datos

#Corriente
I1_file=open("Corriente_I1", "w+")
I2_file=open("Corriente_I2", "w+")
I3_file=open("Corriente_I3", "w+")
I_media_file=open("Corriente_media", "w+")

#Voltaje
V_L1_L2_file=open("Voljate_L1_L2", "w+")
V_L2_L3_file=open("Voljate_L2_L3", "w+")
V_L3_L1_file=open("Voljate_L3_L1", "w+")
V_L_L_media_file=open("Voljate_L_L_media", "w+")
V_L1_N_file=open("Voljate_L1_N", "w+")
V_L2_N_file=open("Voljate_L2_N", "w+")
V_L3_N_file=open("Voljate_L3_N", "w+")
V_L_N_media_file=open("Voljate_L_N_media", "w+")

#Potencia
PA_1_file=open("Potencia_Activa_F1", "w+")
PA_2_file=open("Potencia_Activa_F2", "w+")
PA_3_file=open("Potencia_Activa_F3", "w+")
PA_total_file=open("Potencia_Activa_total", "w+")
P_Reactiva_file=open("Potencia_Reactiva_total", "w+")
P_Aparente_file=open("Potencia_Aparente_total", "w+")

#Factor de potencia
Factor_Potencia_file=open("Factor_Potencia", "w+")

# Frecuencia
Frecuencia_file=open("Frecuencia", "w+")



def leer_registros (direccion,tamano,ID):
    request = client.read_holding_registers(direccion,tamano,unit=ID)
    respuesta= client.execute(request)
    dato_1=request.registers[0]
    dato_2=request.registers[1]
    dato_1=bin((0x10000 | dato_1))
    dato_2=bin(0x10000 | dato_2)
    dato_1_list=list(dato_1)
    dato_2_list=list(dato_2)
    dato_list = dato_1_list+dato_2_list
    dato_list.pop(0)
    dato_list.pop(0)
    dato_list.pop(0)
    dato_list.pop(17)
    dato_list.pop(17)
    dato_list.pop(17)
    return conversion(dato_list)
   
def stamp_time():
    now=datetime.now()
    Year=str(now.year)
    Mes=str(now.month)
    Dia=str(now.day)
    Hr=str(now.hour)
    Minuto=str(now.minute)
    Segundo=str(now.second)
    MicroSegundo=str(now.microsecond)
    date= Year+','+Mes+','+Dia+','+Hr+':'+Minuto+':'+Segundo+'.'+MicroSegundo
    return  date
        
try :   
    while True:
        #Corriente Fase 1
    
    #leer_registros(address, size_of_bytes_to_read, Device_ID)
        I1= leer_registros(2999,2,4)
        I1_file.write("%s," %stamp_time()+  "%f \r\n" %I1 )
    
        #Corriente Fase 2
        I2= leer_registros(3001,2,4)
        I2_file.write("%s," %stamp_time()+  "%f \r\n" %I2)
        #Corriente Fase 3       
        I3= leer_registros(3003,2,4)
        I3_file.write("%s," %stamp_time()+  "%f \r\n" %I3)
        #Corriente media
        I_media= leer_registros(3003,2,4)
        I_media_file.write("%s," %stamp_time()+  "%f \r\n" %I_media)


        # Voltaje  L1-L2
        V_L1_L2= leer_registros(3019,2,4)
        V_L1_L2_file.write("%s," %stamp_time()+  "%f \r\n" %V_L1_L2)
        # Voltaje L2-L3
        V_L2_L3= leer_registros(3021,2,4)
        V_L2_L3_file.write("%s," %stamp_time()+  "%f \r\n" %V_L2_L3)
        # Voltaje L3-L1
        V_L3_L1= leer_registros(3023,2,4)
        V_L3_L1_file.write("%s," %stamp_time()+  "%f \r\n" %V_L3_L1)
        # Voltaje L-L media
        V_L_L= leer_registros(3025,2,4)
        V_L_L_media_file.write("%s," %stamp_time()+  "%f \r\n" %V_L_L)
        #Voltaje L1-N
        V_L1_N= leer_registros(3027,2,4)
        V_L1_N_file.write("%s," %stamp_time()+  "%f \r\n" %V_L1_N)
        #Voltaje L2_N
        V_L2_N= leer_registros(3029,2,4)
        V_L2_N_file.write("%s," %stamp_time()+  "%f \r\n" %V_L2_N)
        #Voltaje L3-N
        V_L3_N=leer_registros(3031,2,4)
        V_L3_N_file.write("%s," %stamp_time()+  "%f \r\n" %V_L3_N)

        #Voltaje L-N media
        V_L_N_media= leer_registros(3035,2,4)
        V_L_N_media_file.write("%s," %stamp_time()+  "%f \r\n" %V_L_N_media)



        #Potencia activa Fase 1
        PA_1= leer_registros(3053,2,4)
        PA_1_file.write("%s," %stamp_time()+  "%f \r\n" %PA_1)
        #Potencia activa Fase 2
        PA_2= leer_registros(3055,2,4)
        PA_2_file.write("%s," %stamp_time()+  "%f \r\n" %PA_2)
        #Potencia activa Fase 3
        PA_3= leer_registros(3057,2,4)
        PA_3_file.write("%s," %stamp_time()+  "%f \r\n" %PA_3)
        #Potencia activa total
        PA_total= leer_registros(3059,2,4)
        PA_total_file.write("%s," %stamp_time()+  "%f \r\n" %PA_total)
        #Potencia Reactiva total (No aplicable al iEM3150 /iEM3250 /iE; 3350)
        P_Reactiva_total= leer_registros(3067,2,4)
        P_Reactiva_file.write("%s," %stamp_time()+  "%f \r\n" %P_Reactiva_total)

        #Potencia aparente total
        P_Aparente_total= leer_registros(3075,2,4)
        P_Aparente_file.write("%s," %stamp_time()+  "%f \r\n" %P_Aparente_total)


        #Factor de Potencia
        Factor_Potencia= leer_registros(3083,2,4)
        Factor_Potencia_file.write("%s," %stamp_time()+  "%f \r\n" %Factor_Potencia)


        #Frecuencia
        Frecuencia= leer_registros(3109,2,4)
        Frecuencia_file.write("%s," %stamp_time()+  "%f \r\n" %Frecuencia)
        time.sleep(0.003)   


except KeyboardInterrupt:
    #Cerramos los archivos de datos:
    I1_file.close()
    #Closes the underlying socket connection
    client.close()
